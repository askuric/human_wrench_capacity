function [H,d] = hyperplane_shifting_method(A,x_min,x_max)
    % function [H,d] = hyperplane_shifting_method(A,x_min,x_max)

    %% Hyperplane shifting method 
    % Gouttefarde M., Krut S. (2010) Characterization of Parallel Manipulator Available Wrench Set Facets. In: Lenarcic J., Stanisic M. (eds) Advances in Robot Kinematics: Motion in Man and Machine. Springer, Dordrecht

    tol = 10e-15;
    H = [];
    d = [];
    % Combination of n x n-1 columns of A
    C = nchoosek(1:size(A,2),size(A,1)-1);
    for comb=1:size(C,1)
        W = A(:,C(comb,:));
        [~,S,V] = svd(W');
        if size(A,1) - rank(S)==1 
            c = V(:,end)';

            % Check for redundant constraint
            if ~isempty(H)  
                %diff = min(vecnorm(H - c,2,2));
                diff = min(sqrt((sum((H-c).^2,1))));
                if diff < tol && diff > -tol
                    c_exists = true;
                else
                    c_exists = false;
                end
            else 
                c_exists = false;
            end

            % Compute offsets    
            if ~c_exists          
                I = c * A;           
                I_positive = find(I>0);
                I_negative = find(I<0); 
                d_positive = sum(I(I_positive) * max(x_min(I_positive),x_max(I_positive))) + sum(I(I_negative) * min(x_min(I_negative),x_max(I_negative)));
                d_negative = -sum(I(I_negative) * max(x_min(I_negative),x_max(I_negative))) - sum(I(I_positive) * min(x_min(I_positive),x_max(I_positive)));

                % Append constraints
                H = [H; c; - c];
                d = [d; d_positive; d_negative];
            end
        end      
    end
    
    %% Singular case
    if ~isempty(H) && rank(A) < size(A,1)
        assert(size(A,1)>1);        

        % Vertices of b (Ax=b)
        b_vert = A * create_box(size(x_min,1),x_min,x_max)';

        % Determine the flat containing the points, rotate the points
        % into a coordinate plane (perpendicular to dimension coord)
        b_vert_rotated = b_vert;
        R_combined = eye(size(x_min,2),size(x_min,2));
        for coord=size(A,1):-1:rank(A)+1
            % Rotate x to y
            x = H(1,:)';    
            y = zeros(coord,1); 
            y(end) = 1;

            u=x/norm(x);
            v=y-u'*y*u;
            if norm(v)>0
                v=v/norm(v);
                cost=x'*y/norm(x)/norm(y);
                sint=sqrt(1-cost^2);
                R = eye(length(x))-u*u'-v*v' + [u v]* [cost -sint;sint cost] *[u v]';

                % Rotate vertices
                b_vert_rotated = (R * b_vert_rotated);

                % Save R
                R_combined = R * R_combined;
            end
        end        

        % (m-1)-dimensional halfspace representation of b_vert_rotated
        b_vert_lower_dim = b_vert_rotated(1:rank(A),:); 
        if size(b_vert_lower_dim,1)>1
            k = convhulln(b_vert_lower_dim');
            b_vert_conv = b_vert_lower_dim(:,unique(k));
            [H_,d_new] = vert2con(b_vert_conv');
        else
            [~,I_min] = min(b_vert_lower_dim');
            [~,I_max] = max(b_vert_lower_dim');
            H_ = [-1;
                  1];
            d_new = [-b_vert_lower_dim(I_min);
                     b_vert_lower_dim(I_max)];
        end
        
        % Remove duplicates within tolerance
        unique_rows = uniquetol(H_,tol,'ByRows',true);    
        [~,~,matching_ind] = intersect(unique_rows,H_,'rows');
        H_unique = H_(matching_ind,:);
        d_new = d_new(matching_ind);
        
        % Rotate m-dimensional version of H_unique by R_combined'
        H_new = (R_combined' * [H_unique,zeros(size(H_unique,1),size(A,1)-rank(A))]')';
        
        % Append new constraints 
        H = [H;H_new];
        d = [d;d_new];    
        
        % Check that each vertex in b_vert satisfies the constraints
        assert(all(all(H * b_vert <= (d + tol))),'Constraints are invalid'); 
    end
end