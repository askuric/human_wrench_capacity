# On-line feasible wrench polytope evaluation based on human musculoskeletal models: an iterative convex hull method

*by Antun Skuric, Vincent Padois, Nasser Rezzoug, David Daney* <br> Submitted to RA-L & ICRA2022

<img src="images/bimanual1.png" height="300">

A new efficient polytope evaluation algorithm for feasible wrench analysis of the human musculoskeletal models.

This repository consists of 
- Paper
    - submitted pdf version of the paper
- Matlab implementation
    - Iterative Convex hull method implementation
    - demo presenting the comparison of three vertex search algorithms for different model sizes
- Python module for calculating human capacity `pycapacity_human`
    - wrench/force polytope
    - acceleration polytope
    - static optimisation to find the bias muscular forces from specified join torques

    
<iframe width="560" height="315" src="https://www.youtube.com/embed/wg4E62AkNnM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Python `pycapacity_human` module

Python module for calculating force capacity based on the human musculoskeletal model 

To obtain the module, the best way is to download it from the [link](https://gitlab.inria.fr/askuric/pycapacity_human), or by using the terminal:
```terminal
cd your_project_path
git clone git@gitlab.inria.fr:askuric/pycapacity_human.git
```

Then you can import it directly as
```python
import pycapacity_human.pycapacity_human as capacity
```

## Module Functions

Polytope calculations:
- [`polytope_acceleration`](https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/docs/pycapacity_human.md#function-polytope_acceleration): A function calculating the polytopes of achievable accelerations
- [`polytope_force`](https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/docs/pycapacity_human.md#function-polytope_force): A function calculating the polytopes of achievable foreces based 
- [`polytope_joint_torques`](https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/docs/pycapacity_human.md#function-polytope_joint_torques): A function calculating the polytopes of achievable joint torques

Base algorithms:
- [`iterative_convex_hull_method`](https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/docs/pycapacity_human.md#function-extended_convex_hull_method): A function calculating the polytopes of a form:
- [`hyper_plane_shift_method`](https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/docs/pycapacity_human.md#function-hyper_plane_shift_method): Hyper plane shifting method implementation used to solve problems of a form:

Utility methods:
- [`torque_to_muscle_force`](https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/docs/pycapacity_human.md#function-torque_to_muscle_force): A function calculating muscle forces needed to create the joint torques tau

---
